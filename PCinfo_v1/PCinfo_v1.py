from psutil import virtual_memory
from hurry.filesize import size
import shutil, os, sys, time, platform

#Coded by @Th3Tr1ckst3r on Twitter.

def PCinfo():
	if sys.platform == "win32":
		os.system("cls")
		os.system("color 0a")
		total, used, free = shutil.disk_usage('/')
		Total = total / 2**30
		Used = used / 2**30
		Free = free / 2**30
		print("\n")
		print("Your PC's Important Hardware Information:")
		print("\n")
		os.system("wmic cpu get name")
		print("Total Amount of RAM: {}".format(size(virtual_memory().total)))
		print("\n")
		print('Total Disk Space: {} GB'.format(Total))
		print("\n")
		print("Used Disk Space: {} GB".format(Used))
		print("\n")
		print("Free Disk Space: {} GB".format(Free))
		print("\n")
		os.system("pause")
		exit()
	else:
		print("Your OS is not supported...")
		time.sleep(10)
		exit()
		
if __name__ == "__main__":
	PCinfo()